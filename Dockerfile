FROM alpine:3.8
LABEL name="Golang CI-CD" \
    version="1.0.0" \
    org.label-schema.vcs-url="https://gitlab.com/poly.dev2019/go-gitlab-heroku" \
    org.label-schema.vendor="Sutin Injitt"
RUN apk --no-cache add ca-certificates \
    && apk add --update tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && apk del tzdata
COPY main /usr/local/bin/app

EXPOSE 8080
CMD ["/usr/local/bin/app"]
